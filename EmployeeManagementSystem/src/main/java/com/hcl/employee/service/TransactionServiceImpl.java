package com.hcl.employee.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.employee.dao.TransactionRepository;
import com.hcl.employee.model.Employee;

@Service
public class TransactionServiceImpl implements TranactionService {

	@Autowired
	TransactionRepository employeeRepository;

	@Override
	public List<Employee> fetchEmployees() {
		List<Employee> employeeList = new ArrayList<>();
		employeeRepository.findAll().forEach(employeeList::add);
		return employeeList;
	}

	@Override
	public void persistEmployee(Employee employee) {
		employeeRepository.save(employee);
	}

	@Override
	public Employee fetchEmployeeById(Long employeeId) {
		return employeeRepository.findOne(employeeId);
	}

	@Override
	public void mergeEmployee(Employee employee) {
		employeeRepository.save(employee);
	}

	@Override
	public void deleteEmployee(Long employeeId) {
		employeeRepository.delete(employeeId);
	}
}

/**
 * 
 */
package com.hcl.employee.service;

import java.util.List;

import com.hcl.employee.model.Employee;

/**
 * @author Shashidhar
 *
 */
public interface TranactionService {

	List<Employee> fetchEmployees();

	void persistEmployee(Employee employee);

	Employee fetchEmployeeById(Long employeeId);

	void mergeEmployee(Employee employee);

	void deleteEmployee(Long employeeId);

}

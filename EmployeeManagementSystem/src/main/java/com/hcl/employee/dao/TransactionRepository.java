/**
 * 
 */
package com.hcl.employee.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.hcl.employee.model.Employee;

/**
 * @author Shashidhar
 *
 */
@Repository
public interface TransactionRepository extends CrudRepository<Employee, Long> { }
